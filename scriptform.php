<script src='https://www.google.com/recaptcha/api.js'></script>	
	<script src="formulario/component/jquery/jquery-3.2.1.min.js"></script>
	<script>

		$(document).ready(function (e){
			$("#frmContact").on('submit',(function(e){
				e.preventDefault();
				$("#mail-status").hide();
				$('#send-message').hide();
				$('#loader-icon').show();
				$.ajax({
					url: "formulario/contact.php",
					type: "POST",
					dataType:'json',
					data: {
						"name":$('input[name="name"]').val(),
						"email":$('input[name="email"]').val(),
						"phone":$('input[name="phone"]').val(),
						"modelo":$('select[name="modelo"]').val(),
						"servicio":$('select[name="servicio"]').val(),
						"content":$('textarea[name="content"]').val(),
						"g-recaptcha-response":$('textarea[id="g-recaptcha-response"]').val()},				
						success: function(response){
							$("#mail-status").show();
							$('#loader-icon').hide();
							if(response.type == "error") {
								$('#send-message').show();
								$("#mail-status").attr("class","error");				
							} else if(response.type == "message"){
								$('#send-message').hide();
								$("#mail-status").attr("class","success");	
							}
							$("#mail-status").html(response.text);	
							document.getElementById("#frmContact").reset();
						},
						error: function(){} 
					});
			}));
		});
	</script>
	<style>
	#message {  padding: 0px 40px 0px 0px; }
	#mail-status {
		padding: 12px 20px;
		width: 100%;
		display:none; 
		font-size: 1em;
		font-family: "Georgia", Times, serif;
		color: rgb(40, 40, 40);
	}
	.error{background-color: #F7902D;  margin-bottom: 40px;}
	.success{background-color: #48e0a4; }
	.g-recaptcha {margin: 0 0 25px 0;}	  
</style>