<div class="latest-post">
				<div class="title-section">
					<h1>Nuestros <span> Camiones</span></h1>
					<p>Navega a través de la sección para seleccionar el camión de tu preferencia</p>
				</div>
                
                <!-- Inserta esta etiqueta donde quieras que aparezca Botón +1. -->
                <div class="g-plusone" data-size="small" data-annotation="none"></div>
                
				    <div id="owl-demo" class="owl-carousel owl-theme">
          
					<div class="item news-item">
							<img alt="Elf 100 Isuzu" src="images/autos/100.png">
						<p></p>
						<a class="read-more" href="pdfs/100.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>
                    
                    
                    <div class="item news-item">
							<img alt="Elf 200 Isuzu" src="images/autos/200.png">
						<p></p>
						<a class="read-more" href="pdfs/200-300.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>
                    
					<div class="item news-item">
							<img alt="Elf 300 Isuzu" src="images/autos/300.png">
						<p></p>
						<a class="read-more" href="pdfs/200-300.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>                    
                    
					<div class="item news-item">
							<img alt="Elf 400 Isuzu" src="images/autos/400.png">
						<p></p>
						<a class="read-more" href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>
                    
					<div class="item news-item">
							<img alt="Elf 500 Isuzu" src="images/autos/500.png">
						<p></p>
						<a class="read-more" href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>   
                    
					<div class="item news-item">
							<img alt="Elf 600 Isuzu" src="images/autos/600.png">
						<p></p>
						<a class="read-more" href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div> 
                    
					<div class="item news-item">
							<img alt="Elf 600 Bus" src="images/autos/600-bus.png">
						<p></p>
						<a class="read-more" href="pdfs/600-bus.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>        
                    
					<div class="item news-item">
							<img alt="Forward 800 Isuzu" src="images/autos/800.png">
						<p></p>
						<a class="read-more" href="pdfs/800.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div> 
                    
					<div class="item news-item">
							<img alt="Forward 1100 Isuzu  " src="images/autos/1100.png">
						<p></p>
						<a class="read-more" href="pdfs/1100.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>    

					<div class="item news-item">
							<img alt="Forward 1400 Isuzu  " src="images/autos/1400.png">
						<p></p>
						<a class="read-more" href="pdfs/1400.pdf" target="_blank">Ficha técnica <i class="fa fa-arrow-right"></i></a>
					</div>    
                   
				</div>
			</div>

