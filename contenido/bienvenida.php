<!-- welcome-box -->
    <div class="welcome-box">
				<div class="container">
					<h1>Bienvenido a la Web Oficial de  <span>Isuzu FAME Camiones</span></h1><br>
					<p>En nuestra página encontrarás <strong><font color="E51E26">todo</font></strong> lo que tenemos para ofrecerte, como <a href="autos.php" target="_self">nuestros camiones</a>, <a href="servicio.php" target="_self">servicio</a>, <a href="promociones.php" target="_self">promociones</a> ¡y mucho más!</p>
				</div>
			</div>