<?php
require('formulario/constant.php');
?>
<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Refacciones</title>

	<?php include('contenido/head.php'); ?>
	<?php include('scriptform.php'); ?>

    </head>

<body>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

        <?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">         



				<div class="container">

					<h2>Refacciones y Accesorios</h2>

				</div>

			</div>



			<div class="about-box">

				<div class="container">

                <div class="row">

						<div class="col-md-12" align="center">

                        

                        

							<div class="single-project-content">

								<img alt="" src="images/refacciones.jpg">

                         	</div>

                            <p align="justify">

                            Porque su vehículo lo merece. Contamos con un extenso surtido de refacciones originales a los mejores precios del mercado, que nos permiten ofrecerle siempre lo que usted necesita.<br><br>

El departamento de refacciones provee y comercializa las refacciones y accesorios originales mediante mostrador o a través del taller de servicio.

                            </p><br>



<p align="justify">

<strong>Los beneficios de adquirir sus refacciones con nosotros son:</strong><br><br>

- Partes 100% Originales y con garantía.<br>

- Amplio inventario de partes.<br>

- Precios altamente competitivos para todos nuestros clientes.<br>

- Excelente Atención.<br>

- Promociones durante todo el año.<br>

- Servicio de envío por paquetería.<br>

</p>

                                           

							<h3>Cotiza tu refacción</h3>

                            <div class="col-md-3"></div>

                            <div class="col-md-6" align="center">

                                 <div class="container">

					                 <div class="col-md-12" >

								         <?php include('formulario/isuzu-cotizarefacciones-pagina.php'); ?>

                                     </div>

                                 </div>

                             </div>

                    </div>

				</div>

			</div>

            </div>

        </div>



<?php include('contenido/footer.php'); ?>

            </div>

</body>

</html>