<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cita de Servicio</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Isuzu FAME Camiones Morelia. Sitio WEB oficial Isuzu FAME Camiones Morelia. Conoce al grupo automotriz más grande de México y Latinoamérica, con más de 1600 unidades en inventario. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Isuzu Morelia, isuzu fame, isuzu camiones, isuzu fame, fame camiones, camiones de carga, camion carga, camion de trabajo, camion transporte, camion diesel, camion economico, camion morelia, isuzu manantiales, plaza fame, isuzu en morelia, agencia isuzu, agencia isuzu morelia, tienda isuzu, isuzu partes, isuzu refacciones, isuzu nuevos, camiones isuzu nuevos, camiones isuzu seminuevos, camiones isuzu usados, vendo camion isuzu, venta isuzu morelia, venta isuzu michoacan, venta isuzu uruapan, taller de camiones, taller de camiones morelia, taller de isuzu, taller de camiones isuzu, taller de camiones isuzu en morelia, fame, Grupo Fame, Morelia, frame, fame manantiales, isuzu Mexico, camiones, trailer, Nuevos, Seminuevos, Agencia, Servicio, Taller, Hojalatería, hojalateria, Pintura, postventa, isuzu elf 200, isuzu elf 300, isuzu elf 400, isuzu elf 500, isuzu elf 600, isuzu elf 600 bus, elf 600 autobus, elf 200, elf 300, elf 400, elf 500, elf 600, isuzu forward, isuzu forward 800, isuzu forward 110, isuzu forward mexico, isuzu forward morelia, isuzu elf, isuzu elf morelia, forward 800, forward 1100, camion barato morelia, camiones fame, camiones isuzu, isuzu morelia fame, isuzu telefono, isuzu ubicacion, isuzu mapa, isuzu agencia morelia, isuzu telefono morelia, isuzu morelia ubicacion, isuzu morelia mapa, concesionario isuzu, concesionario isuzu morelia, concesionario isuzu mexico, camiones 2015, 2015, camiones nuevos, isuzu nuevos, isuzu usados, isuzu seminuevos">
    <meta name="author" content="Grupo FAME División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							
							<span><i class="fa fa-phone"></i>Agencia: (443) 298 1944</span>

						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/pages/Isuzu-Fame/1554203564847510?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/GrupoFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="whatsapp" href="http://www.famecamiones.com/contacto.php" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
							 <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>

							<li class="drop"><a class="active" href="servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									<li><a href="refacciones.php">Refacciones</a></li>
                                    <li><a href="manejo.php">Cita de Manejo</a></li>
                                    <li><a href="cotiza.php">Cotiza tu camión</a></li>
                                    <li><a href="garantia.html">Extención de garantía</a></li>
                                  </ul> </li> 
                                                                                              
							<li class="drop"><a href="autos.html">Camiones</a>
								<ul class="drop-down">
                                	<li><a href="fichas/100.pdf" target="_blank">Elf 100</a></li>
									<li><a href="fichas/200-300.pdf" target="_blank">Elf 200</a></li>
									<li><a href="fichas/200-300.pdf" target="_blank">Elf 300</a></li>
									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 400</a></li>
									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 500</a></li>																																				 									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 600</a></li>
                                    <li><a href="fichas/600-bus.pdf" target="_blank">Elf 600 Bus</a></li>
                                    <li><a href="fichas/800.pdf" target="_blank">Forward 800</a></li>
									<li><a href="fichas/1100.pdf" target="_blank">Forward 1100</a></li>

                                  </ul> </li>        
                                                  
							<li><a href="promociones.html">Promociones</a></li> 
                                                       
							<li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>
                            
                            <li><a href="blog.html">Blog</a></li>
                            
							<li class="drop"><a href="contacto.php">Contacto</a>
                            	<ul class="drop-down">
                                	<li><a href="contacto.php">Contáctanos</a></li>
                                    
                                    
                                    
                               </ul>
                            </li>
                            <li><a href="ubicacion.html" target="_blank">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
<!-- ANALYTICS CAMIONES-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47755649-1', 'auto');
  ga('send', 'pageview');

</script>



<!-- FIN ANALYTICS -->      

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Agenda tu cita de Servicio</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-12" align="center">
                            
							<div class="single-project-content">
								<img alt="" src="images/servicio-isuzu.jpg">
                         	</div>  
                            
                            <p align="justify">
                            En el departamento de servicio lo recibirá cordialmente nuestro equipo de técnicos profesionales, los cuales lo asesorarán en la revisión y tipo de servicio que su vehículo requiere, para lo cual contamos con los equipos más avanzados para diagnóstico y pronta solución en los procesos de reparación.<br><br>
 Eficiencia, calidad, el menor tiempo posible y los precios más accesibles del mercado en los servicios de mantenimiento preventivo y correctivo, son algunos de los beneficios que ofrecemos a nuestros clientes, de manera que puedan disfrutar plenamente de la seguridad, funcionalidad y diseño que su vehículo les brinda.

                            </p><br><br>                          
        
          <h3>Cita de Servicio</h3>

<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "servicio@famecamiones.com" . ','. "formas@grupofame.com" . ',' . "gerencia@famecamiones.com";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cita Servicio FAME Camiones";
			$asunto = "Cita de Servicio Isuzu Fame Camiones";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <?php if(isset($result)) { echo $result; } ?>
    </form>                            
                            
<p align="justify">*Le recordamos que las citas de manejo y servicio están sujetas a horarios disponibles.</p><br><br><br><br><br>                            
                            
                           
						</div>

					</div>
				</div>
			</div>

		</div>



		<!-- End content -->


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i>  01800 670 8386 | </span>2016 Isuzu FAME Camiones | <i class="fa fa-user"> </i><a href="aviso.html" target="_self"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</body>
</html>