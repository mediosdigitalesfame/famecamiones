<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cotizador de Camión</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Isuzu FAME Camiones Morelia. Sitio WEB oficial Isuzu FAME Camiones Morelia. Conoce al grupo automotriz más grande de México y Latinoamérica, con más de 1600 unidades en inventario. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Isuzu Morelia, isuzu fame, isuzu camiones, isuzu fame, fame camiones, camiones de carga, camion carga, camion de trabajo, camion transporte, camion diesel, camion economico, camion morelia, isuzu manantiales, plaza fame, isuzu en morelia, agencia isuzu, agencia isuzu morelia, tienda isuzu, isuzu partes, isuzu refacciones, isuzu nuevos, camiones isuzu nuevos, camiones isuzu seminuevos, camiones isuzu usados, vendo camion isuzu, venta isuzu morelia, venta isuzu michoacan, venta isuzu uruapan, taller de camiones, taller de camiones morelia, taller de isuzu, taller de camiones isuzu, taller de camiones isuzu en morelia, fame, Grupo Fame, Morelia, frame, fame manantiales, isuzu Mexico, camiones, trailer, Nuevos, Seminuevos, Agencia, Servicio, Taller, Hojalatería, hojalateria, Pintura, postventa, isuzu elf 200, isuzu elf 300, isuzu elf 400, isuzu elf 500, isuzu elf 600, isuzu elf 600 bus, elf 600 autobus, elf 200, elf 300, elf 400, elf 500, elf 600, isuzu forward, isuzu forward 800, isuzu forward 110, isuzu forward mexico, isuzu forward morelia, isuzu elf, isuzu elf morelia, forward 800, forward 1100, camion barato morelia, camiones fame, camiones isuzu, isuzu morelia fame, isuzu telefono, isuzu ubicacion, isuzu mapa, isuzu agencia morelia, isuzu telefono morelia, isuzu morelia ubicacion, isuzu morelia mapa, concesionario isuzu, concesionario isuzu morelia, concesionario isuzu mexico, camiones 2015, 2015, camiones nuevos, isuzu nuevos, isuzu usados, isuzu seminuevos">
    <meta name="author" content="Grupo FAME División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							
							<span><i class="fa fa-phone"></i>Agencia: (443) 298 1944</span>

						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/pages/Isuzu-Fame/1554203564847510?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/GrupoFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="whatsapp" href="http://www.famecamiones.com/contacto.php" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
							 <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>

							<li class="drop"><a class="active" href="servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									<li><a href="refacciones.php">Refacciones</a></li>
                                    <li><a href="manejo.php">Cita de Manejo</a></li>
                                    <li><a href="cotiza.php">Cotiza tu camión</a></li>
                                    
                                  </ul> </li> 
                                                                                              
							<li class="drop"><a href="autos.html">Camiones</a>
								<ul class="drop-down">
                                	<li><a href="fichas/100.pdf" target="_blank">Elf 100</a></li>
									<li><a href="fichas/200-300.pdf" target="_blank">Elf 200</a></li>
									<li><a href="fichas/200-300.pdf" target="_blank">Elf 300</a></li>
									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 400</a></li>
									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 500</a></li>																																				 									<li><a href="fichas/400-500-600.pdf" target="_blank">Elf 600</a></li>
                                    <li><a href="fichas/600-bus.pdf" target="_blank">Elf 600 Bus</a></li>
                                    <li><a href="fichas/800.pdf" target="_blank">Forward 800</a></li>
									<li><a href="fichas/1100.pdf" target="_blank">Forward 1100</a></li>

                                  </ul> </li>        
                                                  
							<li><a href="promociones.html">Promociones</a></li> 
                                                       
							<li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>
                            
                            <li><a href="blog.html">Blog</a></li>
                            
							<li class="drop"><a href="contacto.php">Contacto</a>
                            	<ul class="drop-down">
                                	<li><a href="contacto.php">Contáctanos</a></li>
                                    
                                    
                                    
                               </ul>
                            </li>
                            <li><a href="ubicacion.html" target="_blank">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
<!-- ANALYTICS CAMIONES-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47755649-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- FIN ANALYTICS --> 

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Cotiza tu Camión</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
                

						<div class="col-md-12" align="center">
                        
							<div class="single-project-content">
								<img alt="" src="banners/00.jpg">
                         	</div>
                                                    
							<h3>Cotiza tu próxima unidad</h3>
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "ventas@famecamiones.com, gerencia@famecamiones.com" . ','. "formas@grupofame.com" ;
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cotizador Isuzu FAME Camiones";
			$asunto = "Cotizador Isuzu FAME Camiones";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>

<?php if(isset($result)) { echo $result; } ?>
    </form><br><br><br>

						</div>
                        
<p align="justify">
<h3>Con Isuzu®, las posibilidades son infinitas</h3>
Te presentamos a continuación algunas de las aplicaciones existentes para reacondicionar tu unidad y que cumpla de lleno con tus necesidades, porque estamos convencidos de que tu camión Isuzu FAME te dará todo lo que requieres para impulsar tu negocio.<br><br>
</p>                        

							<div class="single-project-content">
								<img alt="" src="images/aplicaciones.jpg"><br>
<p align="justify">
<strong>*Consulta con nuestros asesores las adaptaciones disponibles para cada modelo de camión.</strong><br>
</p>
                         	</div>
                

			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

 
               </div>
             </div>
 


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i>  01800 670 8386 | </span>2016 Isuzu FAME Camiones | <i class="fa fa-user"> </i><a href="aviso.html" target="_self"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</body>
</html>