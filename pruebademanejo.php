<?php
require('formulario/constant.php');
?>
<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>
	<?php include('scriptform.php'); ?>

</head>



<body>



	<div id="container">

		<?php include('contenido/header.php'); ?>

		<?php include('contenido/analytics.php'); ?>



		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2>Contáctanos</h2>



				</div>

			</div>



			<!-- contact box -->

			<div class="contact-box">

				<div class="container">

					<div class="row">



						<div class="col-md-6" align="center">

							<div class="container">

								<div class="col-md-12" >

									<a href="https://api.whatsapp.com/send?phone=524434716989&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">

										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">

										</i> <font size="6"> WhatsApp</font> 

									</button>

								</a>

							</div>

						</div>



						<br>



						<div class="container">

							<div class="col-md-12" >

								<?php include('formulario/isuzu-pruebademanejo-pagina.php'); ?>

							</div>

						</div>

					</div>



					<div class="col-md-3">

						<div class="contact-information">

							<h3>Información de Contacto</h3>

							<ul class="contact-information-list">

								<li><span><i class="fa fa-home"></i>Periférico Paseo de la República #511</span> <span>Col. Los Manantiales </span> <span> Morelia, Michoacán</span> <span>C.P. 58170</span></li>

								<li><span><i class="fa fa-phone"></i><strong>(443) 298 1944</strong></span></li>

								<li><span>Servicio, Postventa, Ventas y Recepcion. <strong> 

<i class="fa fa-phone"></i> 2981944/45</strong></span>.<br>



									<!-- <i class="fa fa-phone"></i><span>Refacciones <strong>298 1944 y 298 1945</strong></span>.<br> -->

									<!--<i class="fa fa-phone"></i><span>Postventa<strong> </strong></span><strong> 2981944/45.</strong><br> -->

									<!--<i class="fa fa-phone"></i><span>Ventas <strong>2981944 y 298 1945.</strong></span><br> -->                

									<!--<i class="fa fa-phone"></i><span>Recepción <strong>2981944 y 298 1945</strong></span>.<br> -->

								</li>

								<h3>Whatsapp</h3>

								<li>

									<span>

										<i class="fa fa-whatsapp"></i>

										<strong>   

											<a href="https://api.whatsapp.com/send?phone=524432732794 &text=Hola,%20Quiero%20más%20información!" title="Ventas"> 

												Ventas | 4434716989

											</a>
<br>
<i class="fa fa-whatsapp"></i>


<a href="https://api.whatsapp.com/send?phone=524434716989&text=Hola,%20Quiero%20más%20información!" title="Ventas"> 

												Refacciones | 4432732794 

											</a>


										</strong>

									</span>

								</li>

								<!-- <li><a href="#"><i class="fa fa-envelope"></i>contacto@hondacamelinas.com</a></li>-->

							</ul>

						</div>

					</div>



					<div class="col-md-3">

						<div class="contact-information">

							<h3>Horario de Atención</h3>

							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Isuzu FAME Camiones</strong>; te escuchamos y atendemos de manera personalizada. </p>

							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

							<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

						</div>

					</div>



				</div>

			</div>

		</div>



	</div> 



	<br><br><br><br><br><br><br>



	<?php include('contenido/footer.php'); ?>

</div> 			

</body>

</html>