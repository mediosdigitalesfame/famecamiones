<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

 

 <head>

<?php include('seguimientos.php'); ?>

	<title>Autos</title>

	<?php include('contenido/head.php'); ?>  

 </head>

<body>



	

	 <div id="container">

         <?php include('contenido/header.php'); ?>

         <?php include('contenido/analytics.php'); ?>



		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2><strong>Camiones Isuzu®</strong></h2>

		

				</div>

			</div>



			<div class="portfolio-box with-3-col">

				<div class="container">

					<ul class="filter">

						<li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>

						<li><a href="#" data-filter=".elf">Línea ELF</a></li>

						<li><a href="#" data-filter=".fwd">Línea Forward</a></li>



                        

					</ul>



					<div class="portfolio-container">



						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/100.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 100</h5>

								<span><a href="pdfs/100.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>

                        

                        <div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/200.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 200</h5>

								<span><a href="pdfs/200-300.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>

                        

						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/300.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 300</h5>

								<span><a href="pdfs/200-300.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>                        

                        

						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/400.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 400</h5>

								<span><a href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div> 

                        

						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/500.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 500</h5>

								<span><a href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>                        

                                 

						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/600.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 600</h5>

								<span><a href="pdfs/400-500-600.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>       

                        

						<div class="work-post elf">

							<div class="work-post-gal">

								<img alt="" src="images/autos/600-bus.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® ELF 600 BUS</h5>

								<span><a href="pdfs/600-bus.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>    

                        

						<div class="work-post fwd">

							<div class="work-post-gal">

								<img alt="" src="images/autos/800.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® Forward 800</h5>

								<span><a href="pdfs/800.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>

                        

						<div class="work-post fwd">

							<div class="work-post-gal">

								<img alt="" src="images/autos/1100.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® Forward 1100</h5>

								<span><a href="pdfs/1100.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>

						<div class="work-post fwd">

							<div class="work-post-gal">

								<img alt="" src="images/autos/1400.png">

							</div>

							<div class="work-post-content">

								<h5>Isuzu® Forward 1400</h5>

								<span><a href="pdfs/1400.pdf" target="_blank">Ficha técnica</a></span>

							</div>

						</div>



                        						

					</div>

		

				</div>

			</div>



		</div>

    </div>

	 <?php include('contenido/footer.php'); ?>

</body>

</html>